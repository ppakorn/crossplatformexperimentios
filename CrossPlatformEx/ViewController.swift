//
//  ViewController.swift
//  CrossPlatformEx
//
//  Created by ampos on 28/12/2561 BE.
//  Copyright © 2561 ampos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var user = User(name: "Anakin Skywalker",
                    avatarUrl: "https://pbs.twimg.com/profile_images/658625563142156288/H2sSv6IA_400x400.jpg")

    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = user.name
    }

    @IBAction func toDashboard(_ sender: Any) {
        // TODO: go to cross platform dashboard
    }
    
}

