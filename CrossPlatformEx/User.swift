class User {
    var name: String
    var avatarUrl: String
    
    init(name: String, avatarUrl: String) {
        self.name = name
        self.avatarUrl = avatarUrl
    }
}
